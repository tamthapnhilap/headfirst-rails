# README

## Starting

```bash
RAILS_ENV=production rails s -p 3000 -b 0.0.0.0
```

## Test Framework

```bash
bundle add rspec-rails --group=development,test
```

**config/application.rb**

```ruby
    config.generators do |g|
      g.orm             :active_record
      g.test_framework  :rspec
    end

ENV['TMP'] = File.join(Rails.root, 'tmp') 
```

```bash
rails generate rspec:install
bundle exec rspec
```

## SetupDB

```bash
rake secret
RAILS_ENV=production rake db:create
RAILS_ENV=production rake db:setup
RAILS_ENV=production rake db:migrate:status
RAILS_ENV=production rake db:migrate
RAILS_ENV=production rake db:rollback

```

## Rails Credentials Replace Secrets

```bash
EDITOR="vi --wait" bin/rails credentials:edit
EDITOR="subl --wait" bin/rails credentials:edit
bin/rails credentials:show
```

Rails also looks for the master key in ENV["RAILS_MASTER_KEY"], if that’s easier to manage.

You could prepend that to your server’s start command like this:

RAILS_MASTER_KEY=“very-secret-and-secure” server.start

## Rails assets

```bash
$ rake -T | grep assets
rake assets:clean[keep]                 # Remove old compiled assets
rake assets:clobber                     # Remove compiled assets
rake assets:environment                 # Load asset compile environment
rake assets:precompile                  # Compile all the assets named in config.assets.precompile
```

## Uglifier Problem

- https://stackoverflow.com/questions/51291859/rails-5-2-issue-with-jquery-rails

## Rubocop

- https://www.rubydoc.info/github/bbatsov/RuboCop/RuboCop/Cop/Layout/FirstParameterIndentation

## Devise Integration

- https://github.com/plataformatec/devise